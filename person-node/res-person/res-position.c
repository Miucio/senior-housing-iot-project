#include "contiki.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "coap-engine.h"

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void res_event_handler(void);

EVENT_RESOURCE(res_position,
    "type=sens.; title=\"Positioning\"",
    res_get_handler,
    NULL,
    NULL,
    NULL,
    res_event_handler);

extern int x;
extern int y;

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
    char pos[20];
    char pos_x[10] = "X: ";
    sprintf(pos_x, "%s%d", pos_x, x);
    char pos_y[10] = ",Y: ";
    sprintf(pos_y, "%s%d", pos_y, y);
    sprintf(pos, "%s%s", pos_x, pos_y);
    coap_set_header_content_format(response, TEXT_PLAIN);
    coap_set_payload(response, buffer, snprintf((char *)buffer, preferred_size, "%s", (char*) pos));
}

static void res_event_handler(void){
    coap_notify_observers(&res_position);
}
