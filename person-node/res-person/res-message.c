#include "contiki.h"
#include <stdio.h>
#include <string.h>
#include "coap-engine.h"

static void res_post_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

extern coap_resource_t res_message;

RESOURCE(res_message,
    "title=\"Message system\"",
    NULL,
    res_post_handler,
    NULL,
    NULL);

static void res_post_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){

    char message[50];
    const char* final_message = NULL;

    if(coap_get_query_variable(request, "value", &final_message)) {
        memset(message,0,strlen(message));
        strcpy(message, final_message);
        message[strlen(final_message)] = '\0';
        printf("Message: %s\n", message);
        coap_set_status_code(response,CREATED_2_01);
    }else{
        coap_set_status_code(response, BAD_REQUEST_4_00);
    }
}
