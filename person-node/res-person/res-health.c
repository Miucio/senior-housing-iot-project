#include "contiki.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "coap-engine.h"

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void res_event_handler(void);

EVENT_RESOURCE(res_health,
    "title=\"Health conditions status\"",
    res_get_handler,
    NULL,
    NULL,
    NULL,
    res_event_handler);

extern int health;

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
    coap_set_header_content_format(response, TEXT_PLAIN);
    coap_set_payload(response, buffer, snprintf((char *)buffer, preferred_size, "%d%%", (int) health));
}

static void res_event_handler(void){
    coap_notify_observers(&res_health);
}
