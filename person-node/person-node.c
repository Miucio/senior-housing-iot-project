#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "contiki.h"
#include "contiki-net.h"
#include "coap-engine.h"
#include "coap-blocking-api.h"
#include "os/dev/button-hal.h"

#include "dev/button-sensor.h"

#include "config-parameters.h"

/* Log configuration */
#include "coap-log.h"

#define LOG_MODULE "App"
#define LOG_LEVEL  LOG_LEVEL_APP
#define TOGGLE_INTERVAL 10

#define SERVER_EP "coap://[fd00::1]:5683"

char *service_url = "/register";

extern coap_resource_t res_message;
extern coap_resource_t res_health;
extern coap_resource_t res_position;
extern coap_resource_t res_name;
extern coap_resource_t res_button;

PROCESS(person_node,"Person node");
AUTOSTART_PROCESSES(&person_node);

static struct etimer et;
static bool done = false;

int health = MAX_HLT;
int x = INIT_POS;
int y = INIT_POS;
bool btn = false;

void client_chunk_handler(coap_message_t *response) {
    const uint8_t *chunk;

    if (response == NULL) {
        puts("Request timed out\n");
        return;
    }

    int len = coap_get_payload(response, &chunk);
    done = true;

    printf("|%.*s", len, (char *) chunk);
}

PROCESS_THREAD(person_node, ev, data){

    static coap_endpoint_t server_ep;
    static coap_message_t request[1];

    PROCESS_BEGIN();

    coap_activate_resource(&res_name, "name");
    coap_activate_resource(&res_health, "hlt");
    coap_activate_resource(&res_position, "pos");
    coap_activate_resource(&res_message, "mess");
    coap_activate_resource(&res_button,"button");

    coap_endpoint_parse(SERVER_EP, strlen(SERVER_EP), &server_ep);

    coap_init_message(request, COAP_TYPE_CON, COAP_GET,0);
    coap_set_header_uri_path(request, service_url);

    printf("Requesting...\n");
    COAP_BLOCKING_REQUEST(&server_ep, request, client_chunk_handler);

    srand((unsigned)time(NULL));

    while(done == false){
        printf("I retry...\n");
        COAP_BLOCKING_REQUEST(&server_ep, request, client_chunk_handler);
    }

    printf("\n--Done--\n");

    etimer_set(&et, 4 * CLOCK_SECOND);

    while(1) {
        PROCESS_WAIT_EVENT();

        if(ev == PROCESS_EVENT_TIMER && data == &et){

            health = rand() % (MAX_HLT + 1 - MIN_HLT) + MIN_HLT;
            res_health.trigger();

            x = rand() % (MAX_POS + 1 - MIN_POS) + MIN_POS;
            y = rand() % (MAX_POS + 1 - MIN_POS) + MIN_POS;
            res_position.trigger();

            etimer_set(&et, 4 * CLOCK_SECOND);
        }
        if(ev == button_hal_press_event){
          btn = true;
          res_button.trigger();
        }
    }

    PROCESS_END();

}
