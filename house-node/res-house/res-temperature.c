#include "contiki.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "coap-engine.h"

/* Log configuration */
#include "sys/log.h"
#define LOG_MODULE "App"
#define LOG_LEVEL LOG_LEVEL_APP

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void res_event_handler(void);

extern float temperature;
int strpos;

//used by every node to know type of node
EVENT_RESOURCE(res_temperature,
         "title=\"temperature value\"",
    		 res_get_handler,
         NULL,
         NULL,
         NULL,
         res_event_handler);

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
  coap_set_header_content_format(response,TEXT_PLAIN);
  strpos = snprintf((char *)buffer,preferred_size,"%.1f",(float)temperature);
  coap_set_payload(response,buffer,strpos);
}

static void res_event_handler(void){
  coap_notify_observers(&res_temperature);
}
