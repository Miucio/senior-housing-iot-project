#include "contiki.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "coap-engine.h"

/* Log configuration */
#include "sys/log.h"
#define LOG_MODULE "App"
#define LOG_LEVEL LOG_LEVEL_APP

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

//used by every node to know type of node
RESOURCE(res_name,
         "title=\"House name\"",
    		 res_get_handler,
         NULL,
         NULL,
         NULL);

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
  int len = sizeof("house");
  memcpy(buffer,"house",len);
  coap_set_header_content_format(response,TEXT_PLAIN);
  coap_set_payload(response, buffer, len-1);
}
