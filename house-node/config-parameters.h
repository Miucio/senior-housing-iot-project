/*INIT variables*/

#define INIT_AIR_QUALITY 60

/*RANGE values */

#define MAX_AIR_QUALITY 100
#define MIN_AIR_QUALITY 0


/*INIT variables*/
#define INIT_P_CON 0

/*RANGE values */

#define MAX_POWER_CONS 50
#define MIN_POWER_CONS 120


/*INIT variables*/
#define INIT_TEMP  28

/*RANGE values */

#define MAX_TEMP 35
#define MIN_TEMP 15

/*RANGE LED COlOR values*/

#define RED_MAX 25
#define YL_MIN 26
#define YL_MAX 50
