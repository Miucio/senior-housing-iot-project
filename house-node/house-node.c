#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include "dev/leds.h"
#include "contiki.h"
#include "contiki-net.h"
#include "coap-engine.h"
#include "coap-blocking-api.h"

#include "config-parameters.h"

//todo: se hai tempo aggiungi attuatore per l'aria

/* Log configuration */
#include "coap-log.h"
#define LOG_MODULE "App"
#define LOG_LEVEL  LOG_LEVEL_APP
#define TOGGLE_INTERVAL 10

//check if actuator is on or not

#define SERVER_EP "coap://[fd00::1]:5683"

char *service_url = "/register";
bool active_air_cond = false;
uint8_t led = 0;

extern coap_resource_t res_temperature;
extern coap_resource_t res_air_quality;
extern coap_resource_t res_power_consumptions;
extern coap_resource_t res_air_conditioner;
extern coap_resource_t res_name;
extern coap_resource_t res_led;


PROCESS(house_node,"House node");
AUTOSTART_PROCESSES(&house_node);

static struct etimer et;
static bool done = false;

/*set variables */
float temperature = INIT_TEMP;
float air_quality = INIT_AIR_QUALITY;
float power_cons = INIT_P_CON;


/* This function is will be passed to COAP_BLOCKING_REQUEST() to handle responses. */
void client_chunk_handler(coap_message_t *response) {
    const uint8_t *chunk;

    if (response == NULL) {
        puts("Request timed out");
        return;
    }

    int len = coap_get_payload(response, &chunk);
    done = true;

    printf("|%.*s", len, (char *) chunk);
}

PROCESS_THREAD(house_node, ev, data){

    static coap_endpoint_t server_ep;
    static coap_message_t request[1];      /* This way the packet can be treated as pointer as usual. */

    PROCESS_BEGIN();

    coap_activate_resource(&res_name,"name");
    coap_activate_resource(&res_temperature,"temperature");
    coap_activate_resource(&res_air_quality,"air_quality");
    coap_activate_resource(&res_power_consumptions,"power_cons");
    coap_activate_resource(&res_air_conditioner,"air_cond");
    coap_activate_resource(&res_led,"led");

    coap_endpoint_parse(SERVER_EP, strlen(SERVER_EP), &server_ep);

    coap_init_message(request, COAP_TYPE_CON,COAP_GET,0);
    coap_set_header_uri_path(request, service_url);

    printf("Request...\n");
    COAP_BLOCKING_REQUEST(&server_ep, request, client_chunk_handler);

    srand((unsigned)time(NULL));

    while(done == false){
        printf("I retry...\n");
        COAP_BLOCKING_REQUEST(&server_ep, request, client_chunk_handler);
    }

    printf("\n--Done--\n");

    etimer_set(&et, 4 * CLOCK_SECOND);
    leds_on(LEDS_NUM_TO_MASK(led));

    while(1) {
        PROCESS_WAIT_EVENT();

        if(ev == PROCESS_EVENT_TIMER && data == &et){

            /* randomly setting the parameters */
            air_quality = ((float)rand()/RAND_MAX)*(MAX_AIR_QUALITY - MIN_AIR_QUALITY + 1) + MIN_AIR_QUALITY;
            res_air_quality.trigger();

            if (air_quality  <= RED_MAX){
                led = LEDS_RED;
            }
            else if (air_quality > YL_MIN && air_quality <= YL_MAX){
                led = LEDS_YELLOW;
            }else{
                led = LEDS_GREEN;
            }

            res_led.trigger();

            power_cons += ((float)rand()/RAND_MAX)*(MAX_POWER_CONS - MIN_POWER_CONS + 1) + MIN_POWER_CONS;
            res_power_consumptions.trigger();

            if (active_air_cond == false){
                temperature = ((float)rand()/RAND_MAX)*(MAX_TEMP - MIN_TEMP + 1) + MIN_TEMP;
                res_temperature.trigger();
            }

            etimer_set(&et, 4 * CLOCK_SECOND);
        }
    }

    PROCESS_END();
}
