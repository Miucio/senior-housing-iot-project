package it.unipi.iot.coap.nodes;

import it.unipi.iot.coap.Resource;

import java.util.HashMap;

public class Node implements INode {

    public static String NAME = "/name";
    public static String CORE = "/.well-known/core";

    private HashMap<String, Resource> resources = new HashMap<>();
    private String name;
    protected String URI;

    public void showContent() {
        for (Resource resource : resources.values()) {
            if (resource.getName().equals(CORE) || resource.getName().equals(NAME)) {
                continue;
            }
            resource.printValue();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addResource(Resource resource) {
        resources.put(resource.getName(), resource);
    }

    public void modifyResource(String resourceName, String value) {
        if (resources.containsKey(resourceName)) {
            resources.get(resourceName).value = value;
        }
    }

    public String getURI() {
        return URI;
    }

    public Resource getResource(String resourceName) {
        return resources.get(resourceName);
    }

}
