package it.unipi.iot.coap;

import org.eclipse.californium.core.CoapClient;

public class Resource {

    private String name;
    public String value;
    private CoapClient clientResource;

    public Resource(String name) {
        this.name = name;
    }

    public void printValue() {
        System.out.print("\t" + name.replace("/", "") + "-->" + value + " \n");
    }

    public String getName() {
        return name;
    }

    public CoapClient getClientResource() {
        return clientResource;
    }

    public void setClientResource(CoapClient clientResource) {
        this.clientResource = clientResource;
    }
}
