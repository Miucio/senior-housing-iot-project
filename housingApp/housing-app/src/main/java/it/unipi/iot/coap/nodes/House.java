package it.unipi.iot.coap.nodes;

public class House extends Node implements INode {

    public static String TEMPERATURE = "/temperature";
    public static String AIRQUALITY = "/air_quality";
    public static String POWERCONS = "/power_cons";
    public static String AIRCOND = "/air_cond";
    public static String LED = "/led";

    public House(String URI) {
        this.URI = URI;
    }


}
